﻿using System;
using System.Runtime.Serialization;

namespace StockExchange.Exceptions
{
    public class PageNotReplyException : Exception
    {
        public PageNotReplyException()
        {
        }

        public PageNotReplyException(string message) : base(message)
        {
        }

        public PageNotReplyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PageNotReplyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
