﻿using System;
using System.Runtime.Serialization;

namespace StockExchange.Exceptions
{
    public class IncorrectConfigurationException : Exception
    {
        public IncorrectConfigurationException()
        {
        }

        public IncorrectConfigurationException(string message) : base(message)
        {
        }

        public IncorrectConfigurationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected IncorrectConfigurationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
