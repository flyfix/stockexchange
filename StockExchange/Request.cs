﻿using System;
using System.Threading.Tasks;
using HtmlAgilityPack;
using StockExchange.Exceptions;

namespace StockExchange
{
    public class Request
    {
        public HtmlDocument Get(string url)
        {
            try
            {
                var web = new HtmlWeb();
                var doc = web.Load(url);
                return doc;
            }
            catch (Exception ex)
            {
                throw new PageNotReplyException(ex.Message, ex);
            }
        }
    }
}
