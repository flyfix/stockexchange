﻿using System.Collections.Generic;

namespace StockExchange
{
    public interface IStockExchangesService
    {
        List<StockExchange> GetExchanges(List<StockExchange> exchanges);
        List<StockExchange> InitValues();
    }
}
