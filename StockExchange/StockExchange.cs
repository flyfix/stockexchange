﻿using System;
using System.Globalization;

namespace StockExchange
{
    public class StockExchange
    {
        public StockExchange(string name, string webElementId)
        {
            Name = name;
            WebElementId = webElementId;
        }

        public StockExchange(string name, string webElementId, float newValue, float oldValue) : this(name,webElementId)
        {
            NewValue = newValue;
            OldValue = oldValue;
            TimeStamp = DateTime.Now;
        }

        public override string ToString()
        {
            return $"{Name.PadRight(10)} {NewValue.ToString(CultureInfo.CurrentCulture).PadRight(15)} {TimeStamp}";
        }

        public string Name { get; private set; }
        public string WebElementId { get; private set; }
        public float NewValue { get; set; } = 0;
        public float OldValue { get; set; } = 0;
        public DateTime TimeStamp { get; set; }

    }
}
