﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace StockExchange
{
    public class ExchangesWriter : IDisposable
    {
        private readonly IStockExchangesService _exchangesService;
        private List<StockExchange> _exchanges;
        private readonly List<string> _exchangesToWrite;
        private readonly long _intervalMilisecons;
        private readonly string _outpuFilePath;

        private Timer _readExchangesTimer;
        private Timer _writeExchangesTimer;


        public ExchangesWriter(IStockExchangesService exchangesService, long intervalMilisecons, string outpuFilePath)
        {
            _exchangesService = exchangesService;
            _intervalMilisecons = intervalMilisecons;
            _outpuFilePath = outpuFilePath;
            _exchangesToWrite = new List<string>();
        }

        public void Start()
        {
            _exchanges = _exchangesService.InitValues();
            _readExchangesTimer = new Timer(GetExchanges, true, 0, _intervalMilisecons);
            _writeExchangesTimer = new Timer(SaveExchanges, true, 1000, 5000);
        }

        public void Stop()
        {
            Dispose();
            SaveExchanges(null);
        }

        private void SaveExchanges(Object stateInfo)
        {
            if (_exchangesToWrite.Any())
            {
                try
                {
                    using (StreamWriter file = new StreamWriter(_outpuFilePath, true))
                    {
                        var joinedExchanges = String.Join("\n", _exchangesToWrite);
                        _exchangesToWrite.Clear();
                        file.WriteLine(joinedExchanges);
                    }
                    Console.WriteLine("Exchanges saved");
                }
                catch (Exception ex)
                {
                    Dispose();
                    Console.Write(ex.Message);
                }
            }
        }

        private void GetExchanges(Object stateInfo)
        {
            try
            {
                _exchanges = _exchangesService.GetExchanges(_exchanges);
                var exchangesWithNewValue = _exchanges.Where(e => !e.OldValue.Equals(e.NewValue));
                if (exchangesWithNewValue.Any())
                {
                    _exchangesToWrite.Add(String.Join("\n", exchangesWithNewValue.Select(e => e.ToString())));
                }
                Console.WriteLine("Exchanges loaded");
            }
            catch (Exception ex)
            {
                Dispose();
                Console.Write(ex.Message);
            }
        }

        public void Dispose()
        {
            _readExchangesTimer.Dispose();
            _writeExchangesTimer.Dispose();
        }
    }
}

