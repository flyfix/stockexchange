﻿using System;
using StockExchange.Exceptions;
using static System.Configuration.ConfigurationManager;

namespace StockExchange
{
    class Program
    {
        static long _interval = 0;
        static string _outputFilePath = "";
        static string _stooqUrl = "";

        static void Main(string[] args)
        {
            ExchangesWriter exchangesWriter = null;

            Console.WriteLine("Application is running.\nPress any key to exit.");
            try
            {
                LoadConfiguration();
                exchangesWriter = new ExchangesWriter(new StooqExchangesService(_stooqUrl), _interval,
                   _outputFilePath);
                exchangesWriter.Start();
                Console.ReadKey();
                exchangesWriter.Stop();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                exchangesWriter?.Dispose();
            }
        }


        static void LoadConfiguration()
        {
            _interval = long.Parse(AppSettings["ExchangeDownloadIntervalInMiliseconds"]);
            _outputFilePath = AppSettings["ExchangesOutputFilePath"];
            _stooqUrl = AppSettings["StooqUrl"];
            if (_interval <= 0 || _outputFilePath == null || _stooqUrl == null)
            {
                throw new IncorrectConfigurationException("Incorrect configuration");
            }
        }
    }
}
