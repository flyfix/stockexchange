﻿using System.Collections.Generic;
using System.Globalization;
using HtmlAgilityPack;
using StockExchange.Exceptions;

namespace StockExchange
{
    public class StooqExchangesService : IStockExchangesService
    {
        public StooqExchangesService(string stooqUrl)
        {
            _stooqUrl = stooqUrl;
        }

        private readonly string _stooqUrl;

        public List<StockExchange> InitValues()
        {
            return new List<StockExchange>()
            {
                new StockExchange("WIG", "aq_wig_c2"), // aq_wig#1_c2 Agility pack ignores #1 inside
                new StockExchange("WIG20", "aq_wig20_c2"),
                new StockExchange("WIG Fut", "aq_fw20_c0"),
                new StockExchange("mWIG40", "aq_mwig40_c2"), // aq_mwig40#1_c2  Agility pack ignores #1 inside
                new StockExchange("sWIG80", "aq_swig80_c2")

            };
        }

        public List<StockExchange> GetExchanges(List<StockExchange> exchanges)
        {
            var stooqDocument = new Request().Get(_stooqUrl);
            return ParseExchanges(exchanges, stooqDocument);
        }

        private List<StockExchange> ParseExchanges(List<StockExchange> exchanges, HtmlDocument document)
        {
            var newExchanges = new List<StockExchange>();
            foreach (var exchange in exchanges)
            {

                var element = document.GetElementbyId(exchange.WebElementId);
                if (element == null)
                {
                    throw new ElementNotFoundException($"Element {exchange.WebElementId} not found");
                }

                float parsedValue;
                if (!float.TryParse(element.InnerText, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out parsedValue))
                {
                    parsedValue = 0;
                }

                newExchanges.Add(new StockExchange(exchange.Name, exchange.WebElementId, parsedValue, exchange.NewValue));
            }
            return newExchanges;
        }
    }
}
